#!/usr/bin/env python

import pymva
import sys,os
from sklearn.utils import shuffle
import numpy,math
from ROOT import TGraph, TFile, TTree
import csv

import numpy as np

import tensorflow

        
argc,argv,argp = pymva.tools.ParseConfig(sys.argv)


#Configuration
tool = pymva.kit.MVAKit('Training')
if argp['ntup_prepared']:
    tool.Parser(argc,argv,0)
    tool.isClassification(0)
else:
    tool.Parser(argc,argv)
tool.ReadConf()

if 'path' in argp:
    path = argp['path']
else:
    path = './'

if path[-1] != '/': path += '/'
    
path += 'pnnrew'+pymva.tools.RemoveSpecialChars(tool.GetEngineOpt())+'/'
if not os.path.exists(path):os.makedirs(path)

#Preparing data into new root file
tool.SetFile('pyMVAKit.root')
tool.SetEvents()
tool.CloseFile()

print ('\nReading data from Py ...')
x_train,y_train,w_train=pymva.tools.ReadFile('pyMVAKit.root','TrainTree0',tool.Variables)

print ('  Finish to obtain data ..')
print ('  Total train: %i'%(len(y_train)))
    
x_train_scaled=pymva.tools.ReadAndStdTransformTxT(x_train,save_loc=path+'keras_output/feature_weight/fold0_')

model = tensorflow.keras.models.load_model(path+'keras_output/model/model_0.h5',
                                           custom_objects={'loss':pymva.ml.ExponentialLoss},
                                           compile=True)
    
ypred_train = model.predict(x_train_scaled)

print(ypred_train)
    
if argp['unc']:
    model_dropout = tensorflow.keras.models.load_model(path+'keras_output/model/model_dropout0.h5',
                                                       custom_objects={'loss':pymva.ml.ExponentialLoss},
                                                       compile=True)
    ypred_avg_train,yunc_train = pymva.ml.predict_with_uncertainty(x_train_scaled, model_dropout, n_iter=1000)

    print(ypred_avg_train)
    print(yunc_train)
    ypred_train = numpy.append(ypred_train,ypred_avg_train,axis=1)
    ypred_train = numpy.append(ypred_train,yunc_train,axis=1)


print(ypred_train)

#Create new ntuple
ntup_opt = 'recreate'
pymva.tools.CloneFile('',argp['ntup'],['nominal_Loose'],[ypred_train],'_unc',ntup_opt=ntup_opt,var_name='score_unc',same_path=True)
