#!/usr/bin/env python

import numpy as np
import pymva
import pandas as pd
import sys,os,math
from sklearn.utils import shuffle
from ROOT import TLorentzVector

import random

import matplotlib.pyplot as plt

import tensorflow as tf
import numpy as np
import keras

class PhysicsLayersGenerator(keras.utils.Sequence):
    def __init__(self,
                 data,
                 labels,
                 weights,
                 global_vars: list = ['nBTags_DL1r_70','nJets'],
                 xcoord: tuple[float,float] = (-5.0,5.0),
                 ycoord: tuple[float,float] = (-5.0,5.0),
                 batch_size: int = 128,
                 dim: tuple[int,int,int] = (128,128,4),
                 shuffle: bool = True
                 ):
        self.dim = dim
        self.batch_size = batch_size
        self.labels = labels
        self.data = data
        self.weights = weights
        self.shuffle = shuffle
        self.xmin = xcoord[0]
        self.xmax = xcoord[1]
        self.ymin = ycoord[0]
        self.ymax = ycoord[1]
        self.deltax = (self.xmax-self.xmin)/self.dim[0]
        self.deltay = (self.ymax-self.ymin)/self.dim[0]
        self.data_min: pd.Series = None
        self.data_max: pd.Series = None
        self.is_normalization_set = False
        self.global_vars = global_vars

        # Calculating Nevents per batch
        self.__nevent = len(self.labels)
        self.__nbatch = int(np.floor(self.__nevent / self.batch_size))
        self.__nout = len(self.labels[0])
        self.__nevent_labeled = [0] * self.__nout
        self.__weight_labeled = [0] * self.__nout
        self.__indexes_per_label = [[] for i in range(self.__nout)]
        for index,label in enumerate(self.labels):
            for i in range(self.__nout):
                if label[i] == 1:
                    self.__weight_labeled[i] += self.weights[index]
                    self.__nevent_labeled[i] += 1
                    self.__indexes_per_label[i].append(int(index))
                    break
        self.__nevent_per_batch = [round(e / self.__nbatch) for e in self.__nevent_labeled]
        max_el = np.argmax(self.__nevent_per_batch)
        self.__nevent_per_batch[max_el] += self.batch_size - sum(self.__nevent_per_batch)
        max_nevent = self.__nevent_per_batch[max_el]
        print(f'Total event: {self.__nevent} & total batch: {self.__nbatch}')
        print(f'Total event / label : {self.__nevent_labeled}')
        print(f'Total weight / label : {self.__weight_labeled}')
        print(f'Total event / batch : {self.__nevent_per_batch}')
        print(f'Total label / batch : {sum(self.__nevent_per_batch)}')
        print('Normalizing weights')
        if 0 in self.__nevent_per_batch:
            print('You dont have enough number of events for each label, please consider to increase batch size.')
            exit()
        self.__nweight_labeled = [0] * self.__nout
        for index,label in enumerate(self.labels):
            for i in range(self.__nout):
                if label[i] == 1:
                    self.weights[index] *= (max_nevent / self.__nevent_per_batch[i]) * (self.__nevent_labeled[i] / self.__weight_labeled[i])
                    self.__nweight_labeled[i] += self.weights[index]
                    break        
        print(f'Total normalized weight / label : {self.__nweight_labeled}')
        
        self.indexes = np.arange(len(self.labels))
        self.on_epoch_end()
        print('Physiscs layers generator succesfuly initialized..')
        
    def __len__(self):
        return int(np.floor(len(self.labels) / self.batch_size))

    def __getitem__(self, index):
        # Generate indexes of the batch
        indexes = self.indexes[index*self.batch_size:(index+1)*self.batch_size]

        # Generate data
        X, y, w = self.__data_generation(indexes)

        return X, y, w

    def reset_indexes(self, shuffle=False):
        self.indexes = np.arange(len(self.labels))
        self.shuffle = shuffle
        if self.shuffle == True:
            np.random.shuffle(self.indexes)

    def normalization(self, data_min: pd.Series, data_max: pd.Series):
        if any(x not in self.data.columns.to_list() for x in data_min.index):
            print('Can not normalize with different sets of variables..')
            exit()
        self.data_min = data_min
        self.data_max = data_max
        self.is_normalization_set = True

    def on_epoch_end(self):
        if self.shuffle == True:
            self.indexes = self.shuffle_by_label()
            #print(self.indexes[0*self.batch_size:(0+1)*self.batch_size])
            
    def shuffle_by_label(self):
        # First shuffle indexes
        for i in range(self.__nout):
            np.random.shuffle(self.__indexes_per_label[i])
        # Create new indexes based on nevent / batch
        indexes_tmp = []
        for i in range(self.__nbatch):
            indexes = np.empty(self.batch_size,dtype=int)
            count = 0
            for j in range(self.__nout):
                start = i * self.__nevent_per_batch[j]
                end = (i+1) * self.__nevent_per_batch[j]
                total = end - start
                vector = self.__indexes_per_label[j][start:end]
                if len(vector) != total:
                    start -= int(self.__nevent_labeled[j]/2)
                    end -= int(self.__nevent_labeled[j]/2)
                    vector = self.__indexes_per_label[j][start:end]
                #print(i,j,start,end,total,count,vector)
                indexes[count:count+total] = vector
                count += total
            np.random.shuffle(indexes)
            indexes_tmp.append(indexes)

        return np.concatenate(tuple(indexes_tmp))

    def draw(self):
        for im in np.concatenate((self.indexes[0:self.batch_size],self.indexes[-self.batch_size:])):
            value = im
            image = self.convert_to_image(value)
            #print ('image',image)
            #print(value)
            label = "Signal" if not self.labels[value][8] else "Background"

            plt.imshow( image , cmap = 'tab20b' )
            plt.title(label)
            plt.xlabel("eta")
            plt.ylabel("phi")
            plt.colorbar()
            plt.savefig(f'/eos/user/a/asonay/plot_conv2d/Tphysic_layers_{value}.png')
            plt.close('all')

    def convert(self,index):
        if not self.is_normalization_set:
            print('Normalization has not been set')
            exit()
        X = np.zeros(self.dim, dtype=np.float32)
        lorentz1, lorentz2, lorentzS= TLorentzVector(), TLorentzVector(), TLorentzVector()
        data = self.data.loc[index]
        n_data = len(data)
        for j in range(n_data):
            eta_j = data['eta'].iloc[j]
            phi_j = data['phi'].iloc[j]
            pt_j = data['pt'].iloc[j]
            e_j = data['e'].iloc[j]
            tag_j = data['tag'].iloc[j]
            loc_x = int((eta_j - self.xmin)/self.deltax)
            loc_y = int((phi_j - self.ymin)/self.deltay)
            X[loc_x][loc_y][0] += np.float32(pt_j/self.data_max['pt'])
            X[loc_x][loc_y][1] += np.float32((tag_j+1)/self.data_max['tag'])
            lorentz1.SetPtEtaPhiE(pt_j,eta_j,phi_j,e_j)
            for k in range(n_data):
                eta_k = data['eta'].iloc[k]
                phi_k = data['phi'].iloc[k]
                pt_k = data['pt'].iloc[k]
                e_k = data['e'].iloc[k]
                tag_k = data['tag'].iloc[k]
                deta = (eta_j-eta_k)/2.0
                dphi = (phi_j-phi_k)/2.0
                loc_x = int((deta - self.xmin)/self.deltax)
                loc_y = int((dphi - self.ymin)/self.deltay)
                lorentz2.SetPtEtaPhiE(pt_k,eta_k,phi_k,e_k)
                lorentzS = lorentz1 + lorentz2
                X[loc_x][loc_y][2] += np.float32(0.5*lorentzS.M()/self.data_max['pt'])
                X[loc_x][loc_y][3] += np.float32(0.5*(tag_j+1+tag_k+1)/self.data_max['tag'])

        return X

    def convert_to_image(self,index):
        if not self.is_normalization_set:
            print('Normalization has not been set')
            exit()
        X = np.zeros((self.dim[0],self.dim[1]), dtype=np.float32)
        lorentz1, lorentz2, lorentzS= TLorentzVector(), TLorentzVector(), TLorentzVector()
        data = self.data.loc[index]
        #print(data)
        #print(data[self.global_vars].iloc[0] / self.data_max[self.global_vars])
        n_data = len(data)
        for j in range(n_data):
            eta_j = data['eta'].iloc[j]
            phi_j = data['phi'].iloc[j]
            pt_j = data['pt'].iloc[j]
            e_j = data['e'].iloc[j]
            tag_j = data['tag'].iloc[j]
            loc_x = int((eta_j - self.xmin)/self.deltax)
            loc_y = int((phi_j - self.ymin)/self.deltay)
            X[loc_x][loc_y] += np.float32((tag_j+1)*pt_j/self.data_max['pt'])
            lorentz1.SetPtEtaPhiE(pt_j,eta_j,phi_j,e_j)
            for k in range(n_data):
                eta_k = data['eta'].iloc[k]
                phi_k = data['phi'].iloc[k]
                pt_k = data['pt'].iloc[k]
                e_k = data['e'].iloc[k]
                tag_k = data['tag'].iloc[k]
                deta = (eta_j-eta_k)/2.0
                dphi = (phi_j-phi_k)/2.0
                loc_x = int((deta - self.xmin)/self.deltax)
                loc_y = int((dphi - self.ymin)/self.deltay)
                lorentz2.SetPtEtaPhiE(pt_k,eta_k,phi_k,e_k)
                lorentzS = lorentz1 + lorentz2
                X[loc_x][loc_y] += np.float32(0.5*(tag_j+1+tag_k+1)*lorentzS.M()/self.data_max['pt'])

        return X

    def __data_generation(self, indexes):
        # Initialization
        X = np.empty((self.batch_size, *self.dim), dtype=np.float32)
        y = np.empty((self.batch_size, 9), dtype=int)
        w = np.empty((self.batch_size), dtype=np.float32)

        for i,index in enumerate(indexes):
            # Store class
            y[i,] = self.labels[index]
            # Store weights
            w[i] = self.weights[index]
            # Store inputs
            data = self.data.loc[index]
            X[i,] = self.convert(index)

        return X, y, w
    

def get_model(cnn_shape = (128,128,4), nout = 9):

    # CNN layers
    cnn_input = tf.keras.Input(shape=cnn_shape)
    norm0 = tf.keras.layers.BatchNormalization()(cnn_input)
    # Layer 1
    conv1 = tf.keras.layers.Conv2D(filters = 64, kernel_size=(1, 1), padding = 'Same', activation='relu')(norm0)
    sdout = tf.keras.layers.SpatialDropout2D(0.5)(conv1)
    pool1 = tf.keras.layers.MaxPooling2D(pool_size=(2, 2))(sdout)
    # Layer 2
    conv2 = tf.keras.layers.Conv2D(filters = 32, kernel_size=(3, 3), padding = 'Same', activation='relu')(pool1)
    pool2 = tf.keras.layers.MaxPooling2D(pool_size=(2, 2))(conv2)
    # Layer 3
    conv3 = tf.keras.layers.Conv2D(filters = 32, kernel_size=(3, 3), padding = 'Same', activation='relu')(pool1)
    pool3 = tf.keras.layers.MaxPooling2D(pool_size=(2, 2))(conv3)
    # Layer 4
    conv4 = tf.keras.layers.Conv2D(filters = 4, kernel_size=(5, 5), padding = 'Same', activation='relu')(pool3)
    #norm4 = tf.keras.layers.BatchNormalization()(conv4)
    pool4 = tf.keras.layers.MaxPooling2D(pool_size=(2, 2))(conv4)

    
    flatten = keras.layers.Flatten()(pool4)

    # Fully connect
    hidden1 = tf.keras.layers.Dense(512, activation='relu')(flatten)
    dropout1 = tf.keras.layers.Dropout(0.5)(hidden1)
    hidden2 = tf.keras.layers.Dense(256, activation='relu')(dropout1)
    dropout2 = tf.keras.layers.Dropout(0.3)(hidden2)
    hidden3 = tf.keras.layers.Dense(256, activation='relu')(dropout2)
    output = tf.keras.layers.Dense(nout, activation='sigmoid')(hidden3)

    #model = tf.keras.models.Model(inputs=cnn_input, outputs=output)
    model = tf.keras.applications.Xception(classes=9,classifier_activation="sigmoid",input_shape=(128,128,4),weights=None)
    
    # summarize layers
    print(model.summary())

    return model

argc,argv,argp = pymva.tools.ParseConfig(sys.argv)

#Configuration
tool = pymva.kit.MVAKit('Training')
if argp['ntup_prepared']:
    tool.Parser(argc,argv,0)
    tool.isClassification(0)
else:
    tool.Parser(argc,argv)
tool.ReadConf()

if 'path' in argp:
    path = argp['path']
    if path[-1] != '/': path += '/'
else:
    path = '.'
    path += '/pyKeras'+pymva.tools.RemoveSpecialChars(tool.GetEngineOpt())+'/'

if not os.path.exists(path):os.makedirs(path)

#Preparing data into new root file
ntupleName=path+'pyMVAKit.root'
if argp['ntup_prepared']:
    ntupleName=argp['ntup']
else:
    tool.SetFile(ntupleName)
    tool.SetEvents()
    tool.CloseFile()

#directory to store 
if not os.path.exists(path+'keras_output'):os.makedirs(path+'keras_output')
if not os.path.exists(path+'keras_output/feature_weight'):os.makedirs(path+'keras_output/feature_weight')
if not os.path.exists(path+'keras_output/model'):os.makedirs(path+'keras_output/model')

split_size = tool.NSplit if tool.NSplit > 0 else 1 

for i in range(tool.NSplit):
    print ('\nReading data from Py ...')
    x_train,y_train,w_train=pymva.tools.ReadFile(ntupleName,'TrainTree'+str(i),tool.Variables)
    x_test,y_test,w_test=pymva.tools.ReadFile(ntupleName,'TestTree'+str(i),tool.Variables)
    #Types; pd, np, np
    print ('  Finish to obtain data ..')
    print ('  Total train: %i, total test: %i'%(len(y_train),len(y_test)))

    # Generators
    print ('  Creating generators ..')
    training_generator = PhysicsLayersGenerator(x_train, y_train, w_train)
    validation_generator = PhysicsLayersGenerator(x_test, y_test, w_test)
    variables = ['pt','e','tag','nJets','nBTags_DL1r_70']
    
    training_generator.normalization(x_train[variables].min(),x_train[variables].max())
    validation_generator.normalization(x_train[variables].min(),x_train[variables].max())

    training_generator.draw()
    
    model = get_model()
    tf.keras.utils.plot_model(model, show_shapes=True)

    loss = 'binary_crossentropy'
    callback = tf.keras.callbacks.EarlyStopping(monitor='loss', patience=3, verbose=1, restore_best_weights=True)
    optim = tf.keras.optimizers.Adam(learning_rate=0.001)
    model.compile(loss=loss, optimizer=optim, metrics=['accuracy',tf.keras.metrics.AUC()])

    model.fit(x=training_generator,
              validation_data=validation_generator,
              validation_freq=10,
              use_multiprocessing=True,
              workers=10,
              epochs=30,
              callbacks=[callback],
              verbose=2)

    optim = tf.keras.optimizers.SGD(learning_rate=0.001)
    model.compile(loss=loss, optimizer=optim, metrics=['accuracy',tf.keras.metrics.AUC()])

    model.fit(x=training_generator,
              validation_data=validation_generator,
              validation_freq=10,
              use_multiprocessing=True,
              workers=10,
              epochs=30,
              callbacks=[callback],
              verbose=2)

    model.save(path+'keras_output/model/model_'+str(i)+'.h5')
    """
    model = tf.keras.models.load_model(path+'keras_output/model/model_99.h5')
    print(model.summary())
    """
    
    training_generator.reset_indexes()
    validation_generator.reset_indexes()
    
    ypred_train = model.predict(training_generator)
    ypred_test = model.predict(validation_generator)

    print (ypred_train)
    print (ypred_test)

    pymva.tools.CloneFile(path,ntupleName,[f'TrainTree{i}',f'TestTree{i}'],[ypred_train,ypred_test],f'_clone{i}',ntup_opt='recreate')
    break
    
    """
    #arrange datas
    print ('  Transforming data ..')
    x_train_scaled,x_test_scaled=pymva.tools.StdTransform(x_train,x_test,tool.Variables,save_loc=path+'keras_output/feature_weight/fold'+str(i)+'_')
    print ('  Shuffling data ..')
    x_train_scaled_shuf,y_train,w_train = shuffle(x_train_scaled,y_train,w_train,random_state=0);
    print ('  Scaling weights ..')
    pymva.tools.ScaleWeights(y_train,w_train)
    #train data
    model = pymva.ml.GetKerasModel(tool.NVar,tool.GetArchitectureOpt())
    pymva.ml.TrainKerasModel(path,model,tool.GetEngineOpt(),x_train_scaled_shuf,y_train,w_train)
    ypred_train = model.predict(x_train_scaled)
    ypred_test = model.predict(x_test_scaled)
    #save everything
    model.save(path+'keras_output/model/model_'+str(i)+'.h5')
    pymva.ml.SaveKerasModel(model,path+'keras_output/model/model_'+str(i)+'.txt')
    ntup_opt = 'recreate' if i == 0 else 'update'
    pymva.tools.CloneFile(path,ntupleName,['TrainTree'+str(i),'TestTree'+str(i)],[ypred_train,ypred_test],'_clone',ntup_opt=ntup_opt)
    """
