#include <fstream>
#include <sstream>
#include <iostream>
#include <string>
#include <random>
#include <vector>
#include <stdio.h>
#include <stdlib.h>
#include <iterator>
#include <map>
#include <vector>
#include <stdarg.h>
#include <unistd.h>

#include "TFile.h"
#include "TTree.h"
#include "TBranch.h"
#include "TH1.h"
#include "TH2.h"
#include "TCanvas.h"
#include "TLegend.h"

#include "TSystem.h"
#include "TCut.h"
#include "TROOT.h"
#include "TStyle.h"

#include "high_pt_unc.cpp"

using namespace std;

static float previous_scale_factor=0.0;
static unsigned jet_counter=0;

float getUpSF(unsigned njet, float pt, int pcbt, int truthflav=5, int eigen=0) {
  const vector<btag_high_pt> &result = btag_high_pt_unc_map.at(eigen);
  
  jet_counter++;
  if (jet_counter==njet) {
    previous_scale_factor=0.0;
    jet_counter=0;
  }
  
  float default_factor = 1.0/static_cast<float>(njet);

  float center = -1;
  float error = 0;

  //cout << jet_counter << " " << previous_scale_factor << " " << pt << " " << pcbt << " " << truthflav << endl;

  if (eigen<4 && (pt<400 || truthflav!=5)) {
    return default_factor;
  }
  else if ((eigen>=4 && eigen<6) && (pt<250 || truthflav!=4)) {
    return default_factor;
  }
  else if (eigen>=6 && (pt<300 || truthflav==5 || truthflav==4)) {
    return default_factor;
  }
  else {
    for (const auto &x : result) {
      if (pt>=x.pt.first && pt<x.pt.second && x.pcbt == pcbt) {
	center = x.value;
	error = sqrt(x.stat_unc*x.stat_unc+x.syst*x.syst);
	//error = x.stat_unc+x.syst;
	break;
      }
    }
  }

  float value_to_add = (center+error)/center-1.0;
  float scale_factor = default_factor + value_to_add + value_to_add*previous_scale_factor;
  previous_scale_factor += value_to_add + value_to_add*previous_scale_factor;
  /*
  if (center != -1) {
    cout << " Found for : " << jet_counter
	 << " pt=" << pt
	 << " pcbt=" << pcbt
	 << " truthflav=" << truthflav
	 << " center=" << center
	 << " error=" << error
	 << " scale_factor=" << scale_factor
	 << " pre_scale_factor=" << previous_scale_factor << endl;
  }
  */
  
  return scale_factor;
  
}

pair<float,float> getVals(float pt, int pcbt, int eigen) {
  const vector<btag_high_pt> &result = btag_high_pt_unc_map.at(eigen);

  float center = -1;
  float error = 0;

  if (eigen<4 && pt<400) {
    return {center,error};
  }
  else if ((eigen>=4 && eigen<6) && pt<250) {
    return {center,error};
  }
  else if (eigen>=6 && pt<300) {
    return {center,error};
  }
  else {
    for (const auto &x : result) {
      if (pt>=x.pt.first && pt<x.pt.second && x.pcbt == pcbt) {
	center = x.value;
	error = sqrt(x.stat_unc*x.stat_unc+x.syst*x.syst);
	//error = x.stat_unc+x.syst;
	break;
      }
    }
  }
  return {center,error};
}

void high_pt_unc_func() {

  auto x = getVals(400,5,0);

  int bin = 100;
  int xmin = 0;
  int xmax = 3050;

  vector<pair<string,int>> b_eigens = {
    {"b0",0},{"b1",1},{"b2",2},{"b3",3},
    {"c0",4},{"c1",5},
    {"light0",6},{"light1",7},{"light2",8},{"light5",9},
  };

  for (pair<string,int> &eigen : b_eigens) {
    vector<TH1F*> hvec;
    TLegend *l = new TLegend(0.5,0.7,0.95,0.95);
    l->SetLineWidth(0);
    l->SetHeader(("Eigen val : "+eigen.first).c_str());
    for (int pcbt : {1,2,3,4,5}) {
      string name = "hpt_"+eigen.first+"_pcbt_"+to_string(pcbt);
      TH1F *h = new TH1F(name.c_str(),"",bin,xmin,xmax);

      for (int i=0;i<bin;i++) {
	float pt = h->GetBinCenter(i+1);
	auto vals = getVals(pt,pcbt,eigen.second);
	h->SetBinContent(i+1,vals.first);
	h->SetBinError(i+1,vals.second);
      }

      h->SetFillColor(kAzure+pcbt); h->SetFillStyle(1001); h->SetMarkerSize(0);
      h->SetMarkerColor(kAzure+pcbt); h->SetLineWidth(0);
      l->AddEntry(h,("PCBT = "+to_string(pcbt)).c_str(),"f");
      hvec.push_back(h);
    }
    TCanvas *c = new TCanvas(("c_"+eigen.first).c_str());
    c->SetTickx(); c->SetTicky();

    string hcan_name = "hcan_"+eigen.first;
    TH2F *hcan = new TH2F(hcan_name.c_str(),"",100,xmin,xmax,100,0,2.55);
    hcan->GetXaxis()->SetTitle("p_{T} GeV");
    hcan->GetYaxis()->SetTitle("Scale Factor");
    hcan->Draw();
    for (auto *h : hvec){
      h->Draw("E2 same");
    }
    l->Draw();
    c->SaveAs(("plot_"+eigen.first+".png").c_str());
    delete c,l;
  }
}
