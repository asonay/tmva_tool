#include <iostream>
#include <algorithm>
#include <vector>
#include <map>
#include <tuple>

using namespace std;

struct OBJs
{
  int pcb;
  float pt;
  float eta;
};

vector<OBJs> objs_vec;
int ttbar_type = 0;
int mult = 0;

bool fillIntoVector(int n, int pcb, float pt, float eta, int type) {
  ttbar_type = type;
  
  if (mult == 0) {
    objs_vec.clear();
  }
  
  OBJs objs_el = {pcb,pt,eta};
  objs_vec.push_back(objs_el);
  mult++;
  
  if (mult == n) {
    mult=0;
    return true;
  }else {
    return false;
  }
}

float getHFTagW() {
  
  float maxsyst = -100.0;
  float syst = 0;
  float maxpt  = 1000.0e3;
  float refpt  = 0;

  for (auto &obj : objs_vec) {
    int pcb   = obj.pcb;
    float eta = obj.eta;
    float pt  = obj.pt;
    float syst_tmp = -100.0;

    // ttb
    if (ttbar_type == 1) {
      refpt = 400.0e3;
      if(pt>refpt){
	if (pcb == 1) {syst_tmp = -15;}
	if (pcb == 2) {syst_tmp = 5;}
	if (pcb == 3) {syst_tmp = 10;}
	if (pcb == 4) {syst_tmp = 15;}
	if (pcb == 5) {syst_tmp = 17;}
      }
    }
    // ttc
    else if (ttbar_type == 2) {
      refpt = 250.0e3;
      if(pt>refpt){
	if (pcb == 1) {syst_tmp = -8;}
	if (pcb == 2) {syst_tmp = 10;}
	if (pcb == 3) {syst_tmp = 30;}
	if (pcb == 4) {syst_tmp = 35;}
	if (pcb == 5) {syst_tmp = 40;}
      }
    }
    // ttl
    else if (ttbar_type == 3) {
      refpt = 300.0e3;
      if(pt>refpt){
	if (pcb == 1) {syst_tmp = 0.1;}
	if (pcb == 2) {syst_tmp = -7;}
	if (pcb == 3) {syst_tmp = -7;}
	if (pcb == 4) {syst_tmp = -8;}
	if (pcb == 5) {syst_tmp = -20;}
      }
    }
    else {
      return 1.0;
    }

    if (syst_tmp > maxsyst) {
      maxsyst = syst_tmp;
    
      if(pt < maxpt){
	syst = maxsyst*(pt - refpt)/(maxpt - refpt);
      }
      else{
	syst = maxsyst;	  
      }
    }

    //cout << ttbar_type << " " << pcb << " " << (1.0+syst/100.) << " " << syst_tmp << " " << maxsyst << " " << pt << " " << refpt << " " << objs_vec.size() << endl;
  }

  return (1.0+syst/100.);
}
