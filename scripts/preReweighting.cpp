// 0: Nominal ; 1: TTB 4FS; 2: MG5Py8; 3: PhHw
//ttX-Sherpa//
/*std::vector<double> TTL_1L = {0.84, 0.84, 0.95, 0.67};
std::vector<double> TTL_2L = {0.87, 0.87, 0.96, 0.73};
std::vector<double> TTC_1L = {1.61, 1.61, 1.80, 2.26};
std::vector<double> TTC_2L = {1.61, 1.61, 1.74, 2.07};
std::vector<double> TTB_1L = {1.21, 0, 0, 0};
std::vector<double> TTB_2L = {1.21, 0, 0, 0};

//ttX-MG//
std::vector<double> TTL_1L = {0.84, 0.84, 0.95, 0.67};
std::vector<double> TTL_2L = {0.87, 0.87, 0.96, 0.73};
std::vector<double> TTC_1L = {1.61, 1.61, 1.80, 2.26};
std::vector<double> TTC_2L = {1.61, 1.61, 1.74, 2.07};
std::vector<double> TTB_1L = {0, 0, 0, 0};
std::vector<double> TTB_2L = {0, 0, 0, 0};

// 0: Nominal ; 1: TTB 4FS; 2: MG5Py8; 3: PhHw
//ttX-Sherpa//
std::vector<double> ttB_1L = {1.21,1.48,1.53,1.40};
std::vector<double> ttB_2L = {1.21,1.39,1.56,1.50};
std::vector<double> ttb_1L = {1.21,1.22,1.28,1.41};
std::vector<double> ttb_2L = {1.21,1.29,1.24,2.22};
std::vector<double> ttbb_1L = {1.21,1.11,1.25,1.80};
std::vector<double> ttbb_2L = {1.21,1.16,1.20,1.86};
std::vector<double> ttbbb_1L = {1.21,0.80,1.32,2.20};
std::vector<double> ttbbb_2L = {1.21,0.77,1.38,2.01};

//ttX-MG//
std::vector<double> ttB_1L = {1.18,1.45,1.50,1.37};
std::vector<double> ttB_2L = {1.18,1.36,1.52,1.47};
std::vector<double> ttb_1L = {1.18,1.19,1.25,1.38};
std::vector<double> ttb_2L = {1.18,1.25,1.21,2.16};
std::vector<double> ttbb_1L = {1.18,1.09,1.22,1.76};
std::vector<double> ttbb_2L = {1.18,1.13,1.17,1.82};
std::vector<double> ttbbb_1L = {1.18,0.78,1.28,2.15};
std::vector<double> ttbbb_2L = {1.18,0.75,1.35,1.96};*/

//Finalized
std::vector<double> TTL_1L = {0.84, 0.83, 0.94, 0.66};
std::vector<double> TTL_2L = {0.87, 0.87, 0.96, 0.73};
std::vector<double> TTC_1L = {1.61, 1.60, 1.78, 2.21};
std::vector<double> TTC_2L = {1.61, 1.60, 1.78, 2.21};
std::vector<double> TTB_1L = {1.18, 1.17, 1.27, 1.56};
std::vector<double> TTB_2L = {1.18, 1.17, 1.27, 1.56};

std::vector<double> ttB_1L = {1.18,1.17,1.27,1.56};
std::vector<double> ttB_2L = {1.18,1.17,1.27,1.56};
std::vector<double> ttb_1L = {1.18,1.17,1.27,1.56};
std::vector<double> ttb_2L = {1.18,1.17,1.27,1.56};
std::vector<double> ttbb_1L = {1.18,1.17,1.27,1.56};
std::vector<double> ttbb_2L = {1.18,1.17,1.27,1.56};
std::vector<double> ttbbb_1L = {1.18,1.17,1.27,1.56};
std::vector<double> ttbbb_2L = {1.18,1.17,1.27,1.56};

double rew_HF_nominal(int nlep, int hf_class, int truth_corr){
  if(nlep==1){
  	if(hf_class==0 && truth_corr<=2){return TTL_1L[0];}
  	else if(hf_class==-1){return TTC_1L[0];}
  	else if((hf_class == 1 || (hf_class == 0 && truth_corr>2))){return TTB_1L[0];}
  	else {return 0.0;}  	
  }
  else if(nlep==2){
  	if(hf_class==0 && truth_corr<=2){return TTL_2L[0];}
  	else if(hf_class==-1){return TTC_2L[0];}
  	else if((hf_class == 1 || (hf_class == 0 && truth_corr>2))){return TTB_2L[0];}
  	else {return 0.0;}   	
  }
  else {return 0.0;}
}

double rew_HF_4FS(int nlep, int hf_class, int truth_corr){
  if(nlep==1){
  	if(hf_class==0 && truth_corr<=2){return TTL_1L[1];}
  	else if(hf_class==-1){return TTC_1L[1];}
  	else if((hf_class == 1 || (hf_class == 0 && truth_corr>2))){return TTB_1L[1];}
  	else {return 0.0;}  	
  }
  else if(nlep==2){
  	if(hf_class==0 && truth_corr<=2){return TTL_2L[1];}
  	else if(hf_class==-1){return TTC_2L[1];}
  	else if((hf_class == 1 || (hf_class == 0 && truth_corr>2))){return TTB_2L[1];}
  	else {return 0.0;}   	
  }
  else {return 0.0;}
}

double rew_HF_MG5Py8(int nlep, int hf_class, int truth_corr){
  if(nlep==1){
  	if(hf_class==0 && truth_corr<=2){return TTL_1L[2];}
  	else if(hf_class==-1){return TTC_1L[2];}
  	else if((hf_class == 1 || (hf_class == 0 && truth_corr>2))){return TTB_1L[2];}
  	else {return 0.0;}  	
  }
  else if(nlep==2){
  	if(hf_class==0 && truth_corr<=2){return TTL_2L[2];}
  	else if(hf_class==-1){return TTC_2L[2];}
  	else if((hf_class == 1 || (hf_class == 0 && truth_corr>2))){return TTB_2L[2];}
  	else {return 0.0;}   	
  }
  else {return 0.0;}
}

double rew_HF_PhHw(int nlep, int hf_class, int truth_corr){
  if(nlep==1){
  	if(hf_class==0 && truth_corr<=2){return TTL_1L[3];}
  	else if(hf_class==-1){return TTC_1L[3];}
  	else if((hf_class == 1 || (hf_class == 0 && truth_corr>2))){return TTB_1L[3];}
  	else {return 0.0;}  	
  }
  else if(nlep==2){
  	if(hf_class==0 && truth_corr<=2){return TTL_2L[3];}
  	else if(hf_class==-1){return TTC_2L[3];}
  	else if((hf_class == 1 || (hf_class == 0 && truth_corr>2))){return TTB_2L[3];}
  	else {return 0.0;}   	
  }
  else {return 0.0;}
}

double rew_TTB_nominal(int nlep,int hf_class,int hf_simClass,int truth_corr){
  if(nlep==1){
    if((abs(hf_class)>=100)&&(abs(hf_class)<200)) return ttB_1L[0];
    else if(((abs(hf_class)>=1000)&&(abs(hf_class)<1100))||((hf_simClass==0)&&(truth_corr==3))) return ttb_1L[0];
    else if(((abs(hf_class)>=2000)&&(abs(hf_class)<2100))||((hf_simClass==0)&&(truth_corr>3))) return ttbb_1L[0];
    else if(((abs(hf_class)>=1100)&&(abs(hf_class)<2000))||((abs(hf_class)>=200)&&(abs(hf_class)<1000))||(abs(hf_class)>=2100)) return ttbbb_1L[0];
    else return 0;
  }
  else if(nlep==2){
    if((abs(hf_class)>=100)&&(abs(hf_class)<200)) return ttB_2L[0];
    else if(((abs(hf_class)>=1000)&&(abs(hf_class)<1100))||((hf_simClass==0)&&(truth_corr==3))) return ttb_2L[0];
    else if(((abs(hf_class)>=2000)&&(abs(hf_class)<2100))||((hf_simClass==0)&&(truth_corr>3))) return ttbb_2L[0];
    else if(((abs(hf_class)>=1100)&&(abs(hf_class)<2000))||((abs(hf_class)>=200)&&(abs(hf_class)<1000))||(abs(hf_class)>=2100)) return ttbbb_2L[0];
    else return 0;    
  }
  else return 0;
}

double rew_TTB_4FS(int nlep,int hf_class,int hf_simClass,int truth_corr){
  if(nlep==1){
    if((abs(hf_class)>=100)&&(abs(hf_class)<200)) return ttB_1L[1];
    else if(((abs(hf_class)>=1000)&&(abs(hf_class)<1100))||((hf_simClass==0)&&(truth_corr==3))) return ttb_1L[1];
    else if(((abs(hf_class)>=2000)&&(abs(hf_class)<2100))||((hf_simClass==0)&&(truth_corr>3))) return ttbb_1L[1];
    else if(((abs(hf_class)>=1100)&&(abs(hf_class)<2000))||((abs(hf_class)>=200)&&(abs(hf_class)<1000))||(abs(hf_class)>=2100)) return ttbbb_1L[1];
    else return 0;
  }
  else if(nlep==2){
    if((abs(hf_class)>=100)&&(abs(hf_class)<200)) return ttB_2L[1];
    else if(((abs(hf_class)>=1000)&&(abs(hf_class)<1100))||((hf_simClass==0)&&(truth_corr==3))) return ttb_2L[1];
    else if(((abs(hf_class)>=2000)&&(abs(hf_class)<2100))||((hf_simClass==0)&&(truth_corr>3))) return ttbb_2L[1];
    else if(((abs(hf_class)>=1100)&&(abs(hf_class)<2000))||((abs(hf_class)>=200)&&(abs(hf_class)<1000))||(abs(hf_class)>=2100)) return ttbbb_2L[1];
    else return 0;    
  }
  else return 0;
}

double rew_TTB_MG5Py8(int nlep,int hf_class,int hf_simClass,int truth_corr){
  if(nlep==1){
    if((abs(hf_class)>=100)&&(abs(hf_class)<200)) return ttB_1L[2];
    else if(((abs(hf_class)>=1000)&&(abs(hf_class)<1100))||((hf_simClass==0)&&(truth_corr==3))) return ttb_1L[2];
    else if(((abs(hf_class)>=2000)&&(abs(hf_class)<2100))||((hf_simClass==0)&&(truth_corr>3))) return ttbb_1L[2];
    else if(((abs(hf_class)>=1100)&&(abs(hf_class)<2000))||((abs(hf_class)>=200)&&(abs(hf_class)<1000))||(abs(hf_class)>=2100)) return ttbbb_1L[2];
    else return 0;
  }
  else if(nlep==2){
    if((abs(hf_class)>=100)&&(abs(hf_class)<200)) return ttB_2L[2];
    else if(((abs(hf_class)>=1000)&&(abs(hf_class)<1100))||((hf_simClass==0)&&(truth_corr==3))) return ttb_2L[2];
    else if(((abs(hf_class)>=2000)&&(abs(hf_class)<2100))||((hf_simClass==0)&&(truth_corr>3))) return ttbb_2L[2];
    else if(((abs(hf_class)>=1100)&&(abs(hf_class)<2000))||((abs(hf_class)>=200)&&(abs(hf_class)<1000))||(abs(hf_class)>=2100)) return ttbbb_2L[2];
    else return 0;    
  }
  else return 0;
}

double rew_TTB_PhHw(int nlep,int hf_class,int hf_simClass,int truth_corr){
  if(nlep==1){
    if((abs(hf_class)>=100)&&(abs(hf_class)<200)) return ttB_1L[3];
    else if(((abs(hf_class)>=1000)&&(abs(hf_class)<1100))||((hf_simClass==0)&&(truth_corr==3))) return ttb_1L[3];
    else if(((abs(hf_class)>=2000)&&(abs(hf_class)<2100))||((hf_simClass==0)&&(truth_corr>3))) return ttbb_1L[3];
    else if(((abs(hf_class)>=1100)&&(abs(hf_class)<2000))||((abs(hf_class)>=200)&&(abs(hf_class)<1000))||(abs(hf_class)>=2100)) return ttbbb_1L[3];
    else return 0;
  }
  else if(nlep==2){
    if((abs(hf_class)>=100)&&(abs(hf_class)<200)) return ttB_2L[3];
    else if(((abs(hf_class)>=1000)&&(abs(hf_class)<1100))||((hf_simClass==0)&&(truth_corr==3))) return ttb_2L[3];
    else if(((abs(hf_class)>=2000)&&(abs(hf_class)<2100))||((hf_simClass==0)&&(truth_corr>3))) return ttbb_2L[3];
    else if(((abs(hf_class)>=1100)&&(abs(hf_class)<2000))||((abs(hf_class)>=200)&&(abs(hf_class)<1000))||(abs(hf_class)>=2100)) return ttbbb_2L[3];
    else return 0;    
  }
  else return 0;
}
